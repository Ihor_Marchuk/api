﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Intefaces
{
    public interface IRepository<T> where T:class
    {
        Task<T> GetById<TId>(TId id);
        Task<IEnumerable<T>> GetAll();
        Task<IQueryable<T>> GetAllForFiltration();

        Task Create(T item);
        Task Update(T item);
        Task Delete(T item);

        Task SaveAsync();
    }
}
