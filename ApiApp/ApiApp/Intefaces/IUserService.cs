﻿using ApiApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Intefaces
{
    public interface IUserService 
    {

        Task<AppUser> GetById<TId>(TId id);
        Task<IEnumerable<AppUser>> GetAll();
        Task<IQueryable<AppUser>> GetAllForFiltration();

        Task Create(AppUser item);
        Task Update(AppUser item);
        Task Delete(AppUser item);
    }
}
