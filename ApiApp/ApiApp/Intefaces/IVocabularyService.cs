﻿using ApiApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Intefaces
{
    public interface IVocabularyService 
    {
        Task<Word> GetById<TId>(TId id);
        Task<IEnumerable<Word>> GetAll();
        Task<IQueryable<Word>> GetAllForFiltration();

        Task Create(Word item);
        Task Update(Word item);
        Task Delete(Word item);
    }
}
