﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiApp.Services.ExternalApiService;
using ApiApp.Services.ThreadService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiApp.Controllers
{
    [Route("Thread")]
    [ApiController]
    public class ThreadController : ControllerBase
    {
        private readonly IThreadService _thread;

        private readonly ILogger _logger;
        public ThreadController(IThreadService thread)
        {
            _thread = thread;
            _logger = LoggerFactory.Create(build => { build.AddConsole(); })
                .CreateLogger<ThreadController>();
        }

        [HttpGet]
        [Route("One")]
        public async Task<IEnumerable<WordFromOwlBot>> MultiThread(string words)
        {
            DateTime time = DateTime.Now;
            IEnumerable<WordFromOwlBot> owl = await _thread.GetInOneThread(words);
            TimeSpan span = DateTime.Now - time;

            _logger.LogInformation(span.Milliseconds.ToString());
            return owl;
        }

        [HttpGet]
        [Route("two")]
        public IEnumerable<WordFromOwlBot> MultiThread2(string words)
        {
            DateTime time = DateTime.Now;
            IEnumerable<WordFromOwlBot> owl = _thread.GetInTwoThread(words);
            TimeSpan span = DateTime.Now - time;

            _logger.LogInformation(span.Milliseconds.ToString());
            return owl;
        }

        [HttpGet]
        [Route("3")]
        public IEnumerable<WordFromOwlBot> MultiThread3()
        {
            DateTime time = DateTime.Now;
            IEnumerable<WordFromOwlBot> owl = _thread.TaskBasedWordsFromOwlBot();
            TimeSpan span = DateTime.Now - time;

            _logger.LogInformation(span.Milliseconds.ToString());
            return owl;
        }

        [HttpGet]
        [Route("4")]
        public IEnumerable<WordFromOwlBot> MultiThread4()
        {
            DateTime time = DateTime.Now;
            IEnumerable<WordFromOwlBot> owl = _thread.TaskBasedWordsFromOwlBotFunc();
            TimeSpan span = DateTime.Now - time;

            _logger.LogInformation(span.Milliseconds.ToString());
            return owl;
        }

        [HttpGet]
        [Route("5")]
        public IEnumerable<WordFromOwlBot> MultiThread5()
        {
            DateTime time = DateTime.Now;
            IEnumerable<WordFromOwlBot> owl = _thread.ParallelAction();
            TimeSpan span = DateTime.Now - time;

            _logger.LogInformation(span.Milliseconds.ToString());
            return owl;
        }

        [HttpGet]
        [Route("6")]
        public IEnumerable<WordFromOwlBot> MultiThread6()
        {
            DateTime time = DateTime.Now;
            IEnumerable<WordFromOwlBot> owl = _thread.ForParallel();
            TimeSpan span = DateTime.Now - time;

            _logger.LogInformation(span.Milliseconds.ToString());
            return owl;
        }
    }
}