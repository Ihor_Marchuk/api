﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using ApiApp.Intefaces;
using ApiApp.Models;
using ApiApp.Services;
using ApiApp.Services.ExternalApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ApiApp.Controllers
{
    [SwaggerTag("Controller with API CRUD operations")]
    [Produces(MediaTypeNames.Application.Json)]
    [Route("api/[controller]")]
    [ApiController]
    public class VocabularyController : ControllerBase
    {
        private readonly IVocabularyService _vocabularyService;


        private readonly ILogger _logger;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="vocabularyService">Service that perform CRUD operations</param>
        public VocabularyController(IVocabularyService vocabularyService)
        {
            _vocabularyService = vocabularyService;
            _logger = LoggerFactory.Create(build => { build.AddConsole(); })
                .CreateLogger<VocabularyController>();
        }

        #region AUTHORIZEBUG

        /// <summary>
        /// Get method that implements read operation
        /// </summary>
        /// <returns>All words from Database</returns>
        /// <response code="200">Operation succeeded</response>
        /// <response code="500">Error while getting information from database</response>
        [Authorize]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Word>>> Get()
        {
            try
            {
                IEnumerable<Word> words = await _vocabularyService.GetAll();

                _logger.LogInformation("Method get returned {0} elements", words.Count());
                return Ok(words);
            }
            catch (Exception)
            {
                _logger.LogError("Error while getting information from database");
                return StatusCode(500);
            }
        }

        #endregion

        /// <summary>
        /// Get one Word from Database
        /// </summary>
        /// <param name="id">Word Id</param>
        /// <returns>GET Word from Database</returns>
        /// <response code="200">Operation succeeded</response>
        /// <response code="500">Error while getting information from database</response>
        /// <response code="404">Not Found</response>
        [HttpGet("{id}")]
        public async Task<ActionResult<Word>> Get(Guid id)
        {
            try
            {
                Word word = await _vocabularyService.GetById(id);

                if (word == null)
                    return NotFound();

                IOwlBot _owl = HttpContext.RequestServices.GetService<IOwlBot>();

                WordFromOwlBot obj = await  _owl.GetWordAsync(word.WordParent); 

                _logger.LogInformation("Request of item with id:{0} from database", word.Id.ToString());
                return word;
            }
            catch (Exception)
            {
                _logger.LogError("Error while getting information from database");
                return StatusCode(500);
            }
        }

        /// <summary>
        /// New record in Database
        /// </summary>
        /// <param name="word">New Word in Database</param>
        /// <returns>200 OK</returns>
        /// <response code="200">Operation succeeded</response>
        /// <response code="500">Error while adding information from database</response>
        /// <response code="400">Bad Request</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Word>> Post(Word word)
        {

            //IUserService userService = HttpContext.RequestServices.GetService<IUserService>();
            if (word == null)
                return BadRequest();

            try
            {
               // await userService.Create(new AppUser { Age = 22, Name = "Admin", Password = "Z1234565767", Id = (Guid)word.AppUserId });
                await _vocabularyService.Create(word);

                _logger.LogInformation("New word added to database");
                return Ok("Post");
            }
            catch (Exception es)
            {
                var error = es.Message;
                _logger.LogError("Error while adding information from database");
                return StatusCode(500);
            }

        }

        /// <summary>
        /// Delete word by Id from Database
        /// </summary>
        /// <param name="id">Words Id</param>
        /// <returns>200 OK</returns>
        /// <response code="200">Operation succeeded</response>
        /// <response code="500">Error while adding information from database</response>
        /// <response code="404">Not Found</response>
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                Word word = await _vocabularyService.GetById(id);

                if (word == null)
                    return NotFound();

                await _vocabularyService.Delete(word);

                _logger.LogInformation("Word with id:{0} was deleted", word.Id);
                return Ok("Delete");
            }
            catch (Exception)
            {
                _logger.LogError("Error while deleting information from database");
                return StatusCode(500);
            }

        }


        /// <summary>
        /// Update Word by Id
        /// </summary>
        /// <param name="word">Word to Update</param>
        /// <returns>200 OK</returns>
        /// <response code="200">Operation succeeded</response>
        /// <response code="500">Error while adding information from database</response>
        /// <response code="400">Bad Request</response>
        [HttpPut]
        public async Task<ActionResult> Update(Word word)
        {
            if (word == null)
                return BadRequest();

            try
            {
                await _vocabularyService.Update(word);

                _logger.LogInformation("Word with id:{0} was updated", word.Id);
                return Ok("Put");
            }
            catch (Exception)
            {
                _logger.LogError("Error while updating information from database");
                return StatusCode(500);
            }
        }

    }
}