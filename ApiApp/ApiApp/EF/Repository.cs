﻿using ApiApp.Intefaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.EF
{
    public class Repository<T>: IDisposable, IRepository<T> where T:class
    {
        private readonly ApplicationContext context;
        public Repository(ApplicationContext context) => this.context = context;

        public virtual async Task Create(T item) => await context.Set<T>().AddAsync(item);
        public virtual async Task Delete(T item) => await Task.Run(() => context.Set<T>().Remove(item));
        public virtual async Task Update(T item) => await Task.Run(() => context.Set<T>().Update(item));

        public virtual async Task<IEnumerable<T>> GetAll() => await context.Set<T>().ToListAsync();
        public virtual async Task<T> GetById<TId>(TId id) => await context.Set<T>().FindAsync(id);
        public virtual async Task<IQueryable<T>> GetAllForFiltration() => await Task.Run(() => context.Set<T>().AsQueryable());

        public virtual async void Dispose() => await context.DisposeAsync();
        public Task SaveAsync() => context.SaveChangesAsync();
       
    }
}
