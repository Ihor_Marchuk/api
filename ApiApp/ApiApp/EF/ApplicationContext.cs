﻿using ApiApp.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiApp.EF
{
    public sealed class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) 
            : base(options) { Database.EnsureCreated(); }

        public DbSet<Word> Words { get; set; }
        public DbSet<AppUser> Users { get; set; }

    }
}
 