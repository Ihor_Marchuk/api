﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Models
{
    public class Login
    {
        public string EMail { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
    }
}
