﻿using System;
using System.Collections.Generic;

namespace ApiApp.Models
{
    public class AppUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }

        public ICollection<Word> Words { get; set; }
    }
}   
