﻿using System;

namespace ApiApp.Models
{
    public class Word
    {
        public Guid Id { get; set; }
        public string WordParent { get; set; }
        public string WordTranslation { get; set; }

        public Guid? AppUserId { get; set; }
        public virtual AppUser AppUser { get; set; }
    }
}
