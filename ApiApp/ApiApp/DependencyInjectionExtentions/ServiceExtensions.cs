﻿using ApiApp.EF;
using ApiApp.Intefaces;
using ApiApp.Services;
using ApiApp.Services.ExternalApiService;
using ApiApp.Services.ThreadService;
using Microsoft.Extensions.DependencyInjection;

namespace ApiApp.DependencyInjectionExtentions
{
    public static class ServiceExtensions
    {
        public static void AddRepositoryService(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
        }

        public static void AddVocabularyService(this IServiceCollection services)
        {
            services.AddScoped<IVocabularyService, VocabularyService>();
        }
        public static void AddUserService(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
        }

        public static void AddOwlBotService(this IServiceCollection services)
        {
            services.AddTransient<IOwlBot,OwlBotService>();
        }
        public static void AddThreadService(this IServiceCollection services)
        {
            services.AddTransient<IThreadService, ThreadService>();
        }

    }
}
