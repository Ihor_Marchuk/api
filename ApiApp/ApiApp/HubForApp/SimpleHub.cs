﻿using ApiApp.EF;
using ApiApp.Models;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.HubForApp
{
    public class SimpleHub : Hub
    {
        public static int I { get; set; } = 0;

        public static List<string> words = new List<string> { "car", "toy", "box", "new", "list" };
        Dictionary<string, string> pairsWords = new Dictionary<string, string> {
            { "car",   "авто" },
            { "toy","іграшка" },
            { "box","коробка" },
            { "new",  "новий" },
            { "list","список" },
        };

        ApplicationContext context;
        public SimpleHub(ApplicationContext context)
        {
            this.context = context;
        }

        public async Task Check(string mes, string username)
        {
            string translation;
            pairsWords.TryGetValue(words[I], out translation);
            if (mes == translation)
            {
                await Clients.All.SendAsync("Send", "відповів вірно!", username,words[I]);
            }
            else
            {
                await Clients.Caller.SendAsync("Send", "Неправильно спробуйте ще", username,words[I]);
                
            }

            await Clients.Caller.SendAsync("Main", "console");

        }
    }
}
