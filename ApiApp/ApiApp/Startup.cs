using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using ApiApp.DependencyInjectionExtentions;
using ApiApp.EF;
using ApiApp.HubForApp;
using ApiApp.Services;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace ApiApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

       
        public void ConfigureServices(IServiceCollection services)
        {
            string context = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(context));

            services.AddRepositoryService();

            services.AddVocabularyService();
            services.AddUserService();

            services.AddOwlBotService();

            services.AddThreadService();

            services.AddControllers();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.RequireHttpsMetadata = false;
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = AuthOptions.ISSUER,
                        ValidateAudience = true,
                        ValidAudience = AuthOptions.AUDIENCE,
                        ValidateLifetime = true,
                        IssuerSigningKey = AuthOptions.GetKey(),
                        ValidateIssuerSigningKey = true
                    };

                });

            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("ver.1.", new OpenApiInfo
                {
                    Title = "ApiApp",
                    Version = "0.1",
                    Description = "ApiApp doc with Swagger"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                opt.IncludeXmlComments(xmlPath);
                opt.EnableAnnotations();
                
            });

            services.AddHangfire(opt => 
                opt.UseSimpleAssemblyNameTypeSerializer().
                    UseDefaultTypeSerializer().
                    UseMemoryStorage()
            );
            services.AddHangfireServer();

            services.AddSignalR();

            services.AddGrpc();
        }

        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IRecurringJobManager recurringJobManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(e =>
                {
                   e.SwaggerEndpoint("/swagger/ver.1./swagger.json","ApiApp Doc");
                });
            }
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMiddleware<ErrorMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseHangfireDashboard();
            BackgroundJob.Enqueue(() => Console.WriteLine("Test hang fire"));
            BackgroundJob.Schedule(() => Console.WriteLine("in 1 minute"),TimeSpan.FromMinutes(1));
            recurringJobManager.AddOrUpdate("111",
               ()=> SetWord(),"0/5 * * * *");

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<SimpleHub>("/hub");

                endpoints.MapGrpcService<TranslatorService>();

            });
        }


        public void SetWord()
        {
            if (SimpleHub.I == SimpleHub.words.Count-1)
            {
                SimpleHub.I = 0;
            }
            SimpleHub.I++;
        }

    }
}
