﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ApiApp
{
    public class ErrorMiddleware
    {
        private readonly RequestDelegate _next;
        public ErrorMiddleware(RequestDelegate next)
        {
           _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            await _next.Invoke(httpContext);

            switch (httpContext.Response.StatusCode)
            {
                case 404:
                    await httpContext.Response.WriteAsync("404 NOT FOUND");
                    break;
                case 500:
                    await httpContext.Response.WriteAsync("500 INTERNAL SERVER ERROR!");
                    break;
            }
        }
    }
}
