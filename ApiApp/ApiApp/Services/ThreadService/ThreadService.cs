﻿using ApiApp.Services.ExternalApiService;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiApp.Services.ThreadService
{
    public class ThreadService : IThreadService
    {
        private readonly IOwlBot _bot;
        
        public ConcurrentBag<WordFromOwlBot> wordsOut { get; set; }
        public ThreadService(IOwlBot owlBot)
        {
            _bot = owlBot;
            wordsOut =new ConcurrentBag<WordFromOwlBot>();
        }

         static readonly string[] _words = { "car", "toy", "book", "map", "new", "history" };

        public async Task<IEnumerable<WordFromOwlBot>> GetInOneThread(string words)
        {
            string[] Words = words.Split(new char[] { ' ' }, StringSplitOptions.None);

            List<WordFromOwlBot> OwlWords = new List<WordFromOwlBot>();

            for (int i = 0;i < Words.GetLength(0); i++)
            {
                OwlWords.Add( await _bot.GetWordAsync(Words[i]));
            }


            return OwlWords;
        }
        public  IEnumerable<WordFromOwlBot> GetInTwoThread(string words)
        {
            FromToModel model = new FromToModel { From = 0, To = 3 };
            FromToModel model2 = new FromToModel { From = 3, To = _words.GetLength(0) };

            Thread firstThread = new Thread(ThreadOp) { Name = "1" };
            Thread secondThread = new Thread(ThreadOp) { Name = "2" };

            firstThread.Start(model);
            secondThread.Start(model2);

            firstThread.Join();
            secondThread.Join();

            while (wordsOut.Count != _words.GetLength(0) )
            {
                Thread.Sleep(1);
            }
            
            return wordsOut;
        }
        public IEnumerable<WordFromOwlBot> TaskBasedWordsFromOwlBot()
        {
            FromToModel model = new FromToModel { From = 0, To = 3 };
            FromToModel model2 = new FromToModel { From = 3, To = _words.GetLength(0) };

            Task.Run(() => ThreadOp(model)).Wait();
            Task.Run(() => ThreadOp(model2)).Wait();

            return wordsOut;
        }
        public IEnumerable<WordFromOwlBot> TaskBasedWordsFromOwlBotFunc()
        {
            FromToModel model = new FromToModel { From = 0, To = 3 };
            FromToModel model2 = new FromToModel { From = 3, To = _words.GetLength(0) };

            List<WordFromOwlBot> List = Task.Run(() => ThreadOpTaskBased(model)).Result.ToList();
            List<WordFromOwlBot> list = Task.Run(() => ThreadOpTaskBased(model2)).Result.ToList();

            list.AddRange(List);

            return list;
        }
        public IEnumerable<WordFromOwlBot> ParallelAction()
        {
            FromToModel model = new FromToModel { From = 0, To = 2 };
            FromToModel model2 = new FromToModel { From = 2, To = 4 };
            FromToModel model3 = new FromToModel { From = 4, To = _words.GetLength(0) };
            Parallel.Invoke(() => ThreadOp(model), () => ThreadOp(model2), () => ThreadOp(model3));

            return wordsOut;
        }
        public IEnumerable<WordFromOwlBot> ForParallel()
        {
            Parallel.For(0,6,ParallelForDel);

            return wordsOut;
        }

        #region MethodsToDelegate
        private async void ThreadOp(object obj)
        {
            FromToModel model = (FromToModel) obj;
            for (int i = model.From; i < model.To; i++)
            {
                wordsOut.Add(await _bot.GetWordAsync(_words[i]));
            }
        }
        private async void ParallelForDel(int to)
        {
            for (int i = 0; i < to; i++)
            {
                wordsOut.Add(await _bot.GetWordAsync(_words[i]));
            }
        }
        private async Task<IEnumerable<WordFromOwlBot>> ThreadOpTaskBased(object obj)
        {
            FromToModel model = (FromToModel)obj;

            List<WordFromOwlBot> list = new List<WordFromOwlBot>();

            for (int i = model.From; i < model.To; i++)
            {
                list.Add(await _bot.GetWordAsync(_words[i]));
            }

            return list;
        }
        
        #endregion

    }

    public static class ThreadServiceExtension
    {
        public static async void GetInOneThread(this ThreadService service, string word)
        {
            await service.GetInOneThread(word);
        }
        public static void GetInTwoThread(this ThreadService service, string word)
        {
            service.GetInTwoThread(word);
        }
        public static void TaskUsingAction(this ThreadService service)
        {
            service.TaskBasedWordsFromOwlBot();
        }
        public static void TaskUsingFunc(this ThreadService service)
        {
            service.TaskBasedWordsFromOwlBotFunc();
        }
        public static void ParallelInvoke(this ThreadService service)
        {
            service.ParallelAction();
        }
        public static void ParallelFor(this ThreadService service)
        {
            service.ForParallel();
        }
    }
}
