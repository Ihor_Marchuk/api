﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiApp.Services.ExternalApiService;

namespace ApiApp.Services.ThreadService
{
    public interface IThreadService
    {
        Task<IEnumerable<WordFromOwlBot>> GetInOneThread(string words);
        IEnumerable<WordFromOwlBot> GetInTwoThread(string words); 
        ConcurrentBag<WordFromOwlBot> wordsOut { get; set; }
        IEnumerable<WordFromOwlBot> TaskBasedWordsFromOwlBot();
        IEnumerable<WordFromOwlBot> TaskBasedWordsFromOwlBotFunc();
        IEnumerable<WordFromOwlBot> ParallelAction();
        IEnumerable<WordFromOwlBot> ForParallel();
    }
}
