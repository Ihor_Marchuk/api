﻿using ApiApp.Intefaces;
using ApiApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Services
{
    public class UserService : IUserService
    {

        private readonly IRepository<AppUser> repository;
        public UserService(IRepository<AppUser> Repository)
        {
            repository = Repository;
        }

        public async Task Create(AppUser item)
        {

            await repository.Create(item);
            await repository.SaveAsync();

        }

        public async Task Delete(AppUser item)
        {
            await repository.Delete(item);
            await repository.SaveAsync();

        }

        public async Task<IEnumerable<AppUser>> GetAll()
        {
            return await repository.GetAll();
        }

        public async Task<IQueryable<AppUser>> GetAllForFiltration()
        {
            return await repository.GetAllForFiltration();
        }

        public async Task<AppUser> GetById<TId>(TId id)
        {
            return await repository.GetById(id);
        }

        public async Task Update(AppUser item)
        {
            await repository.Update(item);
            await repository.SaveAsync();
        }
    }
}
