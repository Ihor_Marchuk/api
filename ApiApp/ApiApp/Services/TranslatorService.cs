﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiApp.Services.ExternalApiService;
using Grpc.Core;
using Hangfire.Server;

namespace ApiApp.Services
{
    public class TranslatorService : Translator.TranslatorBase
    {
        private readonly IOwlBot _owlBot;

        public TranslatorService(IOwlBot owlBot)
        {
            _owlBot = owlBot;
        }

        public override Task<wordResponse> translateWord(wordRequest request, ServerCallContext context)
        {
            var translatedWord = _owlBot.GetWordAsync(request.Word).Result;

            return Task.FromResult(new wordResponse
            {
                Type = translatedWord.definitions.FirstOrDefault().type,
                Definition = translatedWord.definitions.FirstOrDefault().definition,
                Example = translatedWord.definitions.FirstOrDefault().example,
                Pronunciation = translatedWord.pronunciation
            });
        }

    }
}
