﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Services.ExternalApiService
{
    public interface IOwlBot
    {
        Task<WordFromOwlBot> GetWordAsync(string word);
    }
}
