﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiApp.Services.ExternalApiService
{
        public class WordFromOwlBot
        {
            public Definition[] definitions { get; set; }
            public string word { get; set; }
            public string pronunciation { get; set; }
        }

        public class Definition
        {
            public string type { get; set; }
            public string definition { get; set; }
            public string example { get; set; }
            public string image_url { get; set; }
            public object emoji { get; set; }
        }
}
