﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiApp.Services.ExternalApiService
{
    public class OwlBotService : IOwlBot
    {
        public async Task<WordFromOwlBot> GetWordAsync(string word)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Token b3f07bbbf2a02d12b00853ac0a657f680ca6432a");
            HttpResponseMessage get = await client.GetAsync($"https://owlbot.info/api/v4/dictionary/{word}");
            string str = await get.Content.ReadAsStringAsync();

            WordFromOwlBot obj = JsonConvert.DeserializeObject<WordFromOwlBot>(str);
            
            return obj;
        }
        
    }

    public static class OwlBotServiceExtension
    {
        public static async void GetOwlBotWordAsync(this OwlBotService service, string word)
        {
           await service.GetWordAsync(word);
        }
    }
}
