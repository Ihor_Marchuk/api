﻿using ApiApp.Intefaces;
using ApiApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiApp.Services
{
    public class VocabularyService : IVocabularyService 
    {

        private readonly IRepository<Word> repository;
        public VocabularyService(IRepository<Word> Repository)
        {
            repository = Repository;
        }

        public async Task Create(Word item)
        {
            await repository.Create(item);
            await repository.SaveAsync();

        }

        public async Task Delete(Word item)
        {
            await repository.Delete(item);
            await repository.SaveAsync();

        }

        public async Task<IEnumerable<Word>> GetAll()
        {
            return await repository.GetAll();
        }

        public async Task<IQueryable<Word>> GetAllForFiltration()
        {
            return await repository.GetAllForFiltration();
        }

        public async Task<Word> GetById<TId>(TId id)
        {
            return await repository.GetById(id);
        }

        public async Task Update(Word item)
        {
            await repository.Update(item);
            await repository.SaveAsync();
        }
    }
}

