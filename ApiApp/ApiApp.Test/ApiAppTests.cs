using System;
using ApiApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiApp.Controllers;
using ApiApp.Intefaces;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using ApiApp.Services.ExternalApiService;
using ApiApp.Services.ThreadService;

namespace ApiApp.Test
{
    [TestClass]
    public class ApiAppTests
    {
        private static readonly IEnumerable<Word> _collection = new List<Word>{
            new Word
            {
                Id = new Guid(),
                WordParent = "Car",
                WordTranslation = "����",
                AppUserId = new Guid()
            },
            new Word
            {
                Id = new Guid(),
                WordParent = "toy",
                WordTranslation = "�������",
                AppUserId = new Guid()
            }
        };
        private readonly Task<IEnumerable<Word>> _words = Task.FromResult(_collection);

        public TestContext TestContext { get; set; }

        [TestMethod]
        public async Task GetMethodResult_NotNull()
        {
            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.GetAll()).Returns(_words);
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<IEnumerable<Word>> check = await controller.Get();
            //
            Assert.IsNotNull(check);
        }

        [TestMethod]
        public async Task GetMethodResult_StatusCode_200()
        {
            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.GetAll()).Returns(_words);
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<IEnumerable<Word>> check = await controller.Get();
            //
            var result = check.Result as OkObjectResult;
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public async Task GetMethodResult_Data_Equal()
        {
            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.GetAll()).Returns(_words);
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<IEnumerable<Word>> check = await controller.Get();
            var result = check.Result as OkObjectResult;
            IEnumerable<Word> getResult = (IEnumerable<Word>)result.Value;
            //
            Assert.AreEqual(_collection.ToArray().Length, getResult.ToArray().Length);
        }

        [TestMethod]
        public async Task GetMethodResult_Data_Equal_2()
        {
            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.GetAll()).Returns(_words);
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<IEnumerable<Word>> check = await controller.Get();
            //
            var result = check.Result as OkObjectResult;
            Assert.AreEqual(_collection, result.Value);
        }

        [TestMethod]
        public async Task GetMethodResult_Count_Expected_2()
        {
            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.GetAll()).Returns(_words);
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<IEnumerable<Word>> check = await controller.Get();
            var result = check.Result as OkObjectResult;
            IEnumerable<Word> getResult = (IEnumerable<Word>)result.Value;
            //
            Assert.IsTrue(getResult.ToArray().Count() == 2);
            Assert.IsFalse(getResult.ToArray().Length == 0);
        }

        //[TestMethod]
        //[DataSource("", "Server= DESKTOP-T793S2U\\SQLEXPRESS;Database=ApiApp;Trusted_connection=True;", "dbo.Words", DataAccessMethod.Sequential)]
        //public async Task TestMethod6()
        //{
        //}

        [TestMethod]
        public async Task PostMethodResult_StatusCode_200()
        {
            Word word = new Word
            {
                Id = new Guid(),
                AppUserId = new Guid(),
                WordParent = "task",
                WordTranslation = "��������"
            };

            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.Create(word));

            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<Word> check = await controller.Post(word);
            var result = check.Result as OkObjectResult;
            //
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public async Task PostMethodResult_StatusCode_400()
        {
            Word word = null;

            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.Create(word));

            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<Word> check = await controller.Post(word);
            var result = check.Result as BadRequestResult;
            //
            Assert.IsTrue(result.StatusCode == 400);
        }

        [TestMethod]
        public async Task DeleteMethodResult_StatusCode_404()
        {
            Word word = new Word
            {
                Id = new Guid(),
                AppUserId = new Guid(),
                WordParent = "task",
                WordTranslation = "��������"
            };

            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.Delete(word));
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<Word> check = await controller.Delete(word.Id);
            var result = check.Result as NotFoundResult;
            //
            Assert.IsTrue(result.StatusCode == 404);
        }

        [TestMethod]
        public async Task DeleteMethodResult_StatusCode_200()
        {
            Word word = new Word
            {
                Id = new Guid(),
                AppUserId = new Guid(),
                WordParent = "task",
                WordTranslation = "��������"
            };

            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.Delete(word));
            mock.Setup(e => e.GetById(word.Id)).Returns(Task.FromResult(word));
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<Word> check = await controller.Delete(word.Id);
            var result = check.Result as OkObjectResult;
            //
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public async Task UpdateMethodResult_StatusCode_200()
        {
            Word word = new Word
            {
                Id = new Guid(),
                AppUserId = new Guid(),
                WordParent = "task",
                WordTranslation = "��������"
            };

            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.Update(word));
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<Word> check = await controller.Update(word);
            var result = check.Result as OkObjectResult;
            //
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public async Task UpdateMethodResult_StatusCode_400()
        {
            Word word = null;

            var mock = new Mock<IVocabularyService>();
            mock.Setup(e => e.Update(word));
            VocabularyController controller = new VocabularyController(mock.Object);
            //
            ActionResult<Word> check = await controller.Update(word);
            var result = check.Result as BadRequestResult;
            //
            Assert.IsTrue(result.StatusCode == 400);
        }


        #region Thread Controller Tests

        static readonly IEnumerable<WordFromOwlBot> arr = new[]
        {
            new WordFromOwlBot{definitions = null,pronunciation = null, word = "car",},
            new WordFromOwlBot{definitions = null,pronunciation = null, word = "toy",},
            new WordFromOwlBot{definitions = null,pronunciation = null, word = "book",},
            new WordFromOwlBot{definitions = null,pronunciation = null, word = "map",},
            new WordFromOwlBot{definitions = null,pronunciation = null, word = "new",},
            new WordFromOwlBot{definitions = null,pronunciation = null, word = "history",}

        };

        private Task<IEnumerable<WordFromOwlBot>> OwlWords = Task.FromResult(arr);

        [TestMethod]
        public async Task OneThread_Returns_Collection()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.GetInOneThread("car toy book map new history")).
                Returns(OwlWords);
            var controller = new ThreadController(mock.Object);
            //
            var result = await controller.MultiThread(" ");
            //
            Assert.IsInstanceOfType(result,typeof(IEnumerable<WordFromOwlBot>));
        }

        [TestMethod]
        public void Thread2_Returns_Collection()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.GetInTwoThread("car toy book map new history")).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread2("");
            //
            Assert.IsInstanceOfType(result, typeof(IEnumerable<WordFromOwlBot>));
        }

        [TestMethod]
        public  void TaskAction_Returns_Collection()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.TaskBasedWordsFromOwlBot()).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result =  controller.MultiThread3();
            //
            Assert.IsInstanceOfType(result, typeof(IEnumerable<WordFromOwlBot>));
        }

        [TestMethod]
        public void  TaskFunc_Returns_Collection()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.TaskBasedWordsFromOwlBotFunc()).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result =  controller.MultiThread4();
            //
            Assert.IsInstanceOfType(result, typeof(IEnumerable<WordFromOwlBot>));
        }


        [TestMethod]
        public async Task OneThread_Returns_Collection_notNull()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.GetInOneThread("car toy book map new history")).
                Returns(OwlWords);
            var controller = new ThreadController(mock.Object);
            //
            var result = await controller.MultiThread("car toy book map new history");
            //
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Thread2_Returns_Collection_notNull()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.GetInTwoThread("car toy book map new history")).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread2("");
            //
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TaskAction_Returns_Collection_notNull()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.TaskBasedWordsFromOwlBot()).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread3();
            //
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TaskFunc_Returns_Collection_notNull()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.TaskBasedWordsFromOwlBotFunc()).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread4();
            //
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public async Task OneThread_Returns_Collection_Count()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.GetInOneThread("car toy book map new history")).
                Returns(OwlWords);
            var controller = new ThreadController(mock.Object);
            //
            var result = await controller.MultiThread("car toy book map new history");
            //
            Assert.IsTrue(result.Count() == arr.Count());
        }

        [TestMethod]
        public void Thread2_Returns_Collection_Count()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.GetInTwoThread("car toy book map new history")).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread2("car toy book map new history");
            //
            Assert.IsTrue(result.Count() == arr.Count());
        }

        [TestMethod]
        public void TaskAction_Returns_Collection_Count()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.TaskBasedWordsFromOwlBot()).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread3();
            //
            Assert.IsTrue(result.Count() == arr.Count());
        }

        [TestMethod]
        public void TaskFunc_Returns_Collection_Count()
        {

            var mock = new Mock<IThreadService>();
            mock.Setup(e => e.TaskBasedWordsFromOwlBotFunc()).
                Returns(arr);
            var controller = new ThreadController(mock.Object);
            //
            var result = controller.MultiThread4();
            //
            Assert.IsTrue(result.Count() == arr.Count());
        }
        #endregion


    }
}
