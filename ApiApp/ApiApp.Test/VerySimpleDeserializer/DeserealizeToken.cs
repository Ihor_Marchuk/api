﻿using ApiApp.Test.TestModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiApp.Test.VerySimpleDeserializer
{
    public static class DeserealizeToken
    {
        public static JsonTokenModel DeserializeToken(string str)
        {
            string[] arr = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            
            return new JsonTokenModel() { assess_token = arr[3], username = arr[6] };
        }
    }
}
