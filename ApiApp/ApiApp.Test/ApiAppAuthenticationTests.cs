﻿using ApiApp.EF;
using ApiAppAuthentication.Controllers;
using ApiAppAuthentication.DB;
using ApiAppAuthentication.DB.Repository;
using ApiAppAuthentication.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using ApiApp.Test.TestModels;
using ApiApp.Test.VerySimpleDeserializer;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ApiApp.Test
{
    [TestFixture]
    class ApiAppAuthenticationTests
    {

        Mock<IRepository<Client>> mock;
        AccountController controller;

        [OneTimeSetUp]
        public void SetupMethod()
        {
            mock = new Mock<IRepository<Client>>();
            mock.Setup(m => m.Find("", "")).Returns(new Client
            { Age = 22, Educations = null, Login = "fff@gmail.com", Password = "144115515", Role = "user", Profile = null });
            controller = new AccountController(mock.Object);
        }
        [OneTimeTearDown]
        public void CleanupMethod()
        {
            controller.Dispose();
        }
            
        [TestCase("ain", "3242525")]
        [TestCase("admin","32425252525")]
        public  void Token_Is_JSON_Type (string log,string pass)
        {
            var mockForTestCace = new Mock<IRepository<Client>>();
            mockForTestCace.Setup(m => m.Find(log,pass)).Returns(new Client
            { Age = 22, Educations = null, Login = log, Password = pass, Role = "user", Profile = null });
            var controller = new AccountController(mockForTestCace.Object);
            
            var token = controller.Token(log, pass);

            Assert.IsInstanceOf(typeof(JsonResult), token);
        }

        [Test]
        public void Token_IsNotNull()
        {
            var token = controller.Token("","");

            Assert.IsNotNull(token);
        }

        [Test]
        public void Token_Is_NotNull()
        {
            var token = (JsonResult)controller.Token("", "");
            string str = token.Value.ToString();
            var model = DeserealizeToken.DeserializeToken(str);


            Assert.IsNotNull(model);
        }

        [Test]
        public void Token_Is_Ok_Structure()
        {
            
            var token = (JsonResult)controller.Token("", "");
            string str = token.Value.ToString();
            var model = DeserealizeToken.DeserializeToken(str);
            string[] tokenParts = model.assess_token.Split(new char[] { '.' }, StringSplitOptions.None);

            Assert.IsTrue(tokenParts.Count() == 3);
            
        }

        [Test]
        public void TokenModel_Is_Ok_Structure()
        {
            
            var token = (JsonResult)controller.Token("", "");
            string str = token.Value.ToString();
            var model = DeserealizeToken.DeserializeToken(str);

            Assert.IsNotNull(model.assess_token);
            Assert.IsNotNull(model.username);
        }

        
        [Test]
        public void PrivateMethod_Test_NotOkInfo()
        {
            Type type = typeof(AccountController);
            var account = Activator.CreateInstance(type, mock.Object);
            MethodInfo getuser = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).
                Where(x=>x.Name== "GetUser" && x.IsPrivate).First();

            var claims = (ClaimsIdentity)getuser.Invoke(account, new object[] { "fff@gmail.com", "144115515" });

            Assert.IsNull(claims);
            
        }


        ////////////////////////////////////////////////////////////////////////////////
  
        [Test]
        public async Task ValuesController_Returns_BadRequest()
        {
            var options = new DbContextOptionsBuilder<Context>().UseInMemoryDatabase("ClientsTest").Options;
            using(var context = new Context(options))
            {
                context.Clients.Add(new Client { Age = 22, Educations = null, Login = "ok@gmail.com", Password = "13123141414", Profile = null, Role = "user" });
                context.Clients.Add(new Client { Age = 33, Educations = null, Login = "frefe@gmail.com", Password = "1313141414", Profile = null, Role = "user" });
            }
            using(var context = new Context(options))
            {
                ValuesController controller = new ValuesController(context);
                int num = context.Clients.Count();

                await controller.Register("newone", "qedqdqd");
                int newNum = context.Clients.Count();

                Assert.IsTrue(num + 1 == newNum);
            }
        }


    }
}
