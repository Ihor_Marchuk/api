﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiApp.Test.TestModels
{
    public class JsonTokenModel
    {
        public string assess_token { get; set; }
        public string username { get; set; }
    }
}
