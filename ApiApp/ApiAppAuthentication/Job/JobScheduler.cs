﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAppAuthentication.Job
{
    public class JobScheduler
    {
        public static async void Start()
        {
            IScheduler scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<JustQuartzTry>().Build();

            ITrigger trigger = TriggerBuilder.Create().
                WithIdentity("First").
                StartNow().WithSimpleSchedule(schedule =>
                schedule.WithIntervalInSeconds(20).WithRepeatCount(4)
                ).Build();

            await scheduler.ScheduleJob(job, trigger);
        }

    }
}
