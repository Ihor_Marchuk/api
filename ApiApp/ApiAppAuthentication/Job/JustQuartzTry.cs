﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;

namespace ApiAppAuthentication.Job
{
    public class JustQuartzTry : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() => Console.WriteLine("QUARTZ"));
        }
    }
}
