﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAppAuthentication.Models
{
    public class Profile
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; } 
        public string LastName { get; set; }

        [Required]
        public string ClientLogin { get; set; }
        public Client Client { get; set; }
       
    }
}
