﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAppAuthentication.Models
{
    public class ClientEducation
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string EduInstitution { get; set; }
        public string Degree { get; set; }


        public string ClientLogin { get; set; }
        public Client Client { get; set; }
    }
}
