﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace ApiAppAuthentication
{
    public class AuthenticationOptions
    {
        public const string ISSUER = "ApiAppAuth";
        public const string AUDIENCE = "Client";
        private const string KEY = "Key222546_secret";
        public const int LIFETIME = 100;

        public static SymmetricSecurityKey GetKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
