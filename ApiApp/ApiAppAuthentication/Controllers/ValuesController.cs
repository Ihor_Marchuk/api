﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiAppAuthentication.DB;
using ApiAppAuthentication.Job;
using ApiAppAuthentication.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;

namespace ApiAppAuthentication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly Context _context;

        
        public ValuesController(Context context)
        {
            _context = context;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(string Login,string Password)
        {
            if (await _context.Clients.FindAsync(Login) == null)
            {
                string dbPassword = Convert.ToBase64String(Encoding.UTF8.GetBytes(Password));
                Client client = new Client
                {
                    Login = Login,
                    Password = dbPassword,
                    Role = "user"
                };
                await _context.Clients.AddAsync(client);
                await _context.SaveChangesAsync();
                 
                return Ok();
            }
            return BadRequest();
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetLogin()
        {
            JobScheduler.Start();

            return Ok(User.Identity.Name);
        }

        [Authorize(Roles = "admin")]
        [Route("role")]
        public IActionResult GetRole()
        {
            string name = User.Identity.Name;

            bool role = User.IsInRole("admin");
            var autho = User.Claims.ToList();

            return Ok("Admin");
        }
    }
}