﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ApiAppAuthentication.DB;
using ApiAppAuthentication.DB.Repository;
using ApiAppAuthentication.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace ApiAppAuthentication.Controllers
{
    
    public class AccountController : Controller
    {
        readonly IRepository<Client> repo;
        public AccountController(IRepository<Client> repository)
        {
            repo = repository;
        }

        [HttpPost("/token")]
        public IActionResult Token(string login, string password)
        {
            var user = GetUser(login, password);
            if (user==null)
                return BadRequest(new {errorText = "Wrong Login or Password"});

            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthenticationOptions.ISSUER,
                audience: AuthenticationOptions.AUDIENCE,
                notBefore: now,
                claims:user.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthenticationOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthenticationOptions.GetKey(),
                    SecurityAlgorithms.HmacSha256));

            var encodJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new {assess_token = encodJwt, username = user.Name};

            return new JsonResult(response);

        }

        private ClaimsIdentity GetUser(string login, string password)
        {
            Client user = repo.Find(login, password);

            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType,user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType,user.Role)
                };

                ClaimsIdentity identity = new ClaimsIdentity(
                    claims,"Token",ClaimsIdentity.DefaultNameClaimType,ClaimsIdentity.DefaultRoleClaimType);

                return identity;
            }

            return null;
        }
    }
}