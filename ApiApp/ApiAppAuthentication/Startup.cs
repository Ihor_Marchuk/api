using System;
using System.Threading.Tasks;
using ApiAppAuthentication.DB;
using ApiAppAuthentication.DB.Repository;
using ApiAppAuthentication.Models;
using Coravel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace ApiAppAuthentication
{
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }

        private  IConfiguration Configuration { get;}
        public void ConfigureServices(IServiceCollection services)
        {

            string Path = Configuration.GetConnectionString("UserStore");
            services.AddDbContext<Context>(options => options.UseSqlServer(Path).UseLoggerFactory(EFLF.LF));

            services.AddScoped<IRepository<Client>, Repository>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.RequireHttpsMetadata = false;
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = AuthenticationOptions.ISSUER,
                        ValidateAudience = true,
                        ValidAudience = AuthenticationOptions.AUDIENCE,
                        ValidateLifetime = true,
                        IssuerSigningKey = AuthenticationOptions.GetKey(),
                        ValidateIssuerSigningKey = true
                    };

                });

            services.AddScheduler();

            //services.AddControllersWithViews();
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            var provider = app.ApplicationServices;
            provider.UseScheduler(coravel =>
                coravel.Schedule(() => Console.WriteLine("Simple")).
                EveryThirtySeconds().Weekday());
            provider.UseScheduler(coravel =>
                coravel.ScheduleAsync(async ()=> await Task.Run(()=>Console.WriteLine("async"))).
                    EveryThirtySeconds());


            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}



