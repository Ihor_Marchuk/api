﻿using Microsoft.Extensions.Logging;

namespace ApiAppAuthentication.DB
{
    public static class EFLF
    {
        public static readonly ILoggerFactory LF = LoggerFactory.Create(C => C.AddConsole());

    }
}
