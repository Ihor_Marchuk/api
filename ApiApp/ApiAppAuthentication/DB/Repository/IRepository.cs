﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAppAuthentication.DB.Repository
{
    public interface IRepository <T> where T : class
    {
        IEnumerable<T> GetAll();
        T Find(string login,string password);
        void Add();
        void Delete();
        void Update();
    }
}
