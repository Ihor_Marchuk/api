﻿using ApiAppAuthentication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiAppAuthentication.DB.Repository
{
    public class Repository : IRepository<Client>
    {

        Context context;
        public Repository(Context context)
        {
            this.context = context;
        }

        public void Add()
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }

        public virtual Client Find(string login, string password)
        {
            var DBpassword = Convert.ToBase64String(Encoding.UTF8.GetBytes(password));
            Client user = context.Clients.FirstOrDefault(x => x.Login == login && x.Password == DBpassword);
            return user;
        }

        public IEnumerable<Client> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }
    }
}
