﻿using ApiAppAuthentication.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAppAuthentication.DB.Configs
{
    public class EducationConfiguration : IEntityTypeConfiguration<ClientEducation>
    {
        public void Configure(EntityTypeBuilder<ClientEducation> builder)
        {
            builder.HasOne(p => p.Client).
                WithMany(p => p.Educations).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
