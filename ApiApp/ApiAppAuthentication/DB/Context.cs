﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiAppAuthentication.Models;
using System.Text;
using ApiAppAuthentication.DB.Configs;

namespace ApiAppAuthentication.DB
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClientConfiguration());
            modelBuilder.ApplyConfiguration(new EducationConfiguration());
        }
        public  DbSet<Client> Clients { get; set; }
        public DbSet<ClientEducation> ClientEducations { get; set; }
    }

}

static class DBInit
{
   public static Client[] GetData()
   {
        string dbPassword = Convert.ToBase64String(Encoding.UTF8.GetBytes("12345678"));

        return 
        new Client[]
        {
            new Client{Login = "admin@mail.com",Password = dbPassword, Role = "admin", Age = 22},
            new Client{Login = "ffff@gmail.com",Password = dbPassword, Role = "user", Age = 22}
        };
   }
}
