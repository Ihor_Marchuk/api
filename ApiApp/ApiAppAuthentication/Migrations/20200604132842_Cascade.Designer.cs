﻿// <auto-generated />
using ApiAppAuthentication.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ApiAppAuthentication.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20200604132842_Cascade")]
    partial class Cascade
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ApiAppAuthentication.Models.Client", b =>
                {
                    b.Property<string>("Login")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Role")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Login");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Login = "admin@mail.com",
                            Password = "MTIzNDU2Nzg=",
                            Role = "admin"
                        },
                        new
                        {
                            Login = "ffff@gmail.com",
                            Password = "MTIzNDU2Nzg=",
                            Role = "user"
                        });
                });

            modelBuilder.Entity("ApiAppAuthentication.Models.ClientEducation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClientLogin")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Degree")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("EduInstitution")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Type")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("ClientLogin");

                    b.ToTable("ClientEducations");
                });

            modelBuilder.Entity("ApiAppAuthentication.Models.ClientEducation", b =>
                {
                    b.HasOne("ApiAppAuthentication.Models.Client", "Client")
                        .WithMany("Educations")
                        .HasForeignKey("ClientLogin")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
