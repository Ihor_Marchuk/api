﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiAppAuthentication.Migrations
{
    public partial class OnetoMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientEducation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true),
                    EduInstitution = table.Column<string>(nullable: true),
                    Degree = table.Column<string>(nullable: true),
                    ClientLogin = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientEducation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientEducation_Users_ClientLogin",
                        column: x => x.ClientLogin,
                        principalTable: "Users",
                        principalColumn: "Login",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientEducation_ClientLogin",
                table: "ClientEducation",
                column: "ClientLogin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientEducation");
        }
    }
}
