﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiAppAuthentication.Migrations
{
    public partial class AgeIgnore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Login = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Login);
                });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Login", "Password", "Role" },
                values: new object[] { "admin@mail.com", "MTIzNDU2Nzg=", "admin" });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Login", "Password", "Role" },
                values: new object[] { "ffff@gmail.com", "MTIzNDU2Nzg=", "user" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
