﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiAppAuthentication.Migrations
{
    public partial class Cascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientEducation_Users_ClientLogin",
                table: "ClientEducation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientEducation",
                table: "ClientEducation");

            migrationBuilder.RenameTable(
                name: "ClientEducation",
                newName: "ClientEducations");

            migrationBuilder.RenameIndex(
                name: "IX_ClientEducation_ClientLogin",
                table: "ClientEducations",
                newName: "IX_ClientEducations_ClientLogin");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientEducations",
                table: "ClientEducations",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientEducations_Users_ClientLogin",
                table: "ClientEducations",
                column: "ClientLogin",
                principalTable: "Users",
                principalColumn: "Login",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientEducations_Users_ClientLogin",
                table: "ClientEducations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientEducations",
                table: "ClientEducations");

            migrationBuilder.RenameTable(
                name: "ClientEducations",
                newName: "ClientEducation");

            migrationBuilder.RenameIndex(
                name: "IX_ClientEducations_ClientLogin",
                table: "ClientEducation",
                newName: "IX_ClientEducation_ClientLogin");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientEducation",
                table: "ClientEducation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ClientEducation_Users_ClientLogin",
                table: "ClientEducation",
                column: "ClientLogin",
                principalTable: "Users",
                principalColumn: "Login",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
